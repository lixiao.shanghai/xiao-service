import express from 'express';
import {logger} from '../logger/logger';
import {auth} from '../middlewares/auth';
import {db} from '../postgres/db';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {v4 as uuidv4} from 'uuid';
export const authenticationRouter = express.Router();

export const unauthorizedMessage = {
  id: 'app.unauthorized',
  defaultMessage: 'Unauthorized'
};

authenticationRouter.post('/login', async (req, res, next) => {
  try {
    const {username, password} = req.body;
    if (!username || !password) {
      return res.status(401).send({error: [unauthorizedMessage]});
    }

    const user = await db.oneOrNone(
      'SELECT * FROM user_account where username=${username}',
      {username}
    );
    if (!user) {
      return next({error: [unauthorizedMessage]});
    }
    const verified = await bcrypt.compare(password, user.password_hash);
    if (!verified) {
      return next({error: [unauthorizedMessage]});
    }
    const token = jwt.sign(
      {
        id: user.id,
        username
      },
      process.env.JWT_TOKEN_KEY!,
      {
        expiresIn: '1h'
      }
    );
    user.token = token;
    return res.status(200).json(user);
  } catch (error) {
    logger.error({name: 'authenticationRouter.login', error, req});
    return next(error);
  }
});

authenticationRouter.post('/register', async (req, res, next) => {
  try {
    const {username, password} = req.body;
    if (!username || !password) {
      return next({
        error: [
          {
            id: 'app.register.fail',
            defaultMessage: 'username and password are required'
          }
        ]
      });
    }
    const oldUser = await db.oneOrNone(
      'SELECT * FROM user_account where username=${username}',
      {username}
    );
    if (oldUser) {
      return next({
        error: [
          {id: 'app.register.userExists', defaultMessage: 'Username is taken'}
        ]
      });
    }
    const encryptedPassword = await bcrypt.hash(password, 12);

    const user = await db.one(
      `INSERT INTO user_account (id, username, password_hash) VALUES ($<id>, $<username>, $<encryptedPassword>) RETURNING id, username`,
      {id: uuidv4(), username, encryptedPassword}
    );

    const token = jwt.sign(
      {
        id: user.id,
        username: user.username
      },
      process.env.JWT_TOKEN_KEY!,
      {
        expiresIn: '1h'
      }
    );

    user.token = token;
    return res.status(201).json(user);
  } catch (e) {
    logger.error({error: e});
  }
});
