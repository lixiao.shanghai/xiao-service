import express from 'express';
import {db} from '../postgres/db';
import {logger} from '../logger/logger';
import {auth} from '../middlewares/auth';

export const postRouter = express.Router();

postRouter.post('/', auth, async (req, res, next) => {
  return res.status(200).json({message: 'got to create'});
});
