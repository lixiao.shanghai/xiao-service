import {NextFunction, Request, Response} from 'express';
import jwt from 'jsonwebtoken';

const authenticationFailed = {
  id: 'app.auth.authenticationFailed',
  defaultMessage: 'Authentication failed'
};

export const auth = (req: Request, res: Response, next: NextFunction) => {
  const token =
    req.body.token || req.query.token || req.headers['x-access-token'];
  if (!token) {
    return next({
      error: [authenticationFailed]
    });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_TOKEN_KEY!);
    req.user = decoded;
  } catch (error) {
    return res.status(401).send({error: [authenticationFailed]});
  }

  return next();
};
