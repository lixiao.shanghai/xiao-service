import express, {Application, NextFunction, Request, Response} from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import {migrate} from 'postgres-migrations';
import {authenticationRouter} from './routes/authenticationRouter';
import jwt from 'express-jwt';
import {logger} from './logger/logger';
import {postRouter} from './routes/postRouter';

// run migration scripts
(async () => {
  const dbConfig = {
    database: process.env.POSTGRES_DATABASE!,
    user: process.env.POSTGRES_USER!,
    password: process.env.POSTGRES_PASSWORD!,
    host: process.env.POSTGRES_HOST!,
    port: parseInt(process.env.POSTGRES_PORT!),
    ensureDatabaseExists: true,
    // Default: "postgres"
    // Used when checking/creating "database-name"
    defaultDatabase: 'postgres'
  };
  await migrate(dbConfig, './migrations');
})();

const app: Application = express();
const port = process.env.PORT;

app.disable('x-powered-by');
// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cookieParser());
app.use(cors());

app.get('/', async (req: Request, res: Response): Promise<Response> => {
  return res.status(200).send({
    message: 'Hello World!'
  });
});

app.use('/api', authenticationRouter);
app.use('/api/post', postRouter);

app.use((err: unknown, req: Request, res: Response, next: NextFunction) => {
  logger.error(`default error handler invoked. Error: ${err}`);
  res.status(400).send(err);
});

try {
  app.listen(port, (): void => {
    logger.info(`Connected successfully on port ${port}`);
  });
} catch (error) {
  logger.error(`Error ocurred: ${error}`);
}
