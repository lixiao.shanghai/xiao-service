const initOptions = {noLocking: true}; // https://stackoverflow.com/a/46855481/11192166
const pgp = require('pg-promise')(initOptions);
const options = {
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DATABASE,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
  max: 100
}
export const db = pgp(options);

// Usage: loadScript(path.join(__dirname), './relative/path')
export const loadScript = (fullPath: string) => {
  return new pgp.QueryFile(fullPath, {minify: false});
};