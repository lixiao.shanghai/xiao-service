CREATE TABLE IF NOT EXISTS user_account (
    id UUID PRIMARY KEY,
    username varchar(100) NOT NULL,
    passwordHash varchar(300),
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);