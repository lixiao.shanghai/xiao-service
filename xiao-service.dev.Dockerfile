FROM node:16-alpine3.12 as build
RUN npm i -g npm@8

ENV NODE_ENV=development
# running as root user prior to this
USER node

RUN mkdir -p /home/node/xiao-service
WORKDIR /home/node/xiao-service

COPY --chown=node:node ./package.json ./
RUN npm i -D

COPY --chown=node:node . .

FROM build as xiao-service
CMD ["npm", "run", "dev"]
